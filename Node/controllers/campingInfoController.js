
//const Camp = require('../models/--');

const request = require('request');
const convert = require('xml-js');


var url = process.env.goCampingUri;


var callAPI = (param , callback ) =>{
    console.log(url + param);
    
    request({
        url: url + param,
        method: 'GET'
    }, function (error, response, body) {
        console.log('Status', response.statusCode);
        console.log('Headers', JSON.stringify(response.headers));

        //xml 형식이므로 convert 필요
        var xmlToJson = convert.xml2json(body, {compact: true, spaces: 4});

        callback(JSON.parse(xmlToJson).response.body);
    });
}


exports.getApi = (req,res,next) => {

    var queryParams;
    queryParams = '?' + encodeURIComponent('ServiceKey') + '=' + encodeURIComponent(process.env.goCamingApiKey); /* Service Key*/
    queryParams += '&' + encodeURIComponent('MobileOS') + '=' + encodeURIComponent(process.env.goCamingApiOs); /* */
    queryParams += '&' + encodeURIComponent('MobileApp') + '=' + encodeURIComponent(process.env.goCamingApiApp); /* */

    
    queryParams += '&' + encodeURIComponent('pageNo') + '=' + encodeURIComponent(req.pageNo); /* */
    queryParams += '&' + encodeURIComponent('numOfRows') + '=' + encodeURIComponent(req.numOfRows); /* */
    
    
    callAPI(queryParams , (result) => {
        res.json(result); 
    });


}

