
const db = require('../src/api/mysqldbConn');
const Model = require('../models/mysqlModels/index');

exports.getInfo = (req,res,next) => { 


    db.connect();

    Model.User.findAll()
    .then( result => { 
        console.log(result);
        res.render( 'list', { list : result }) 
    })
    .catch( err => { 
        throw err
    })
    .finally( () => {
        db.close();
    });
    

};
