
const User = require('../models/mongooseModels/userModel');
const db = require('../src/api/mongoConn');



exports.findUser = (req,res,next) => {

    db.createConnection();
    
    User.find({ id:  req.params.id }).exec()
        .then( user => {
            console.log(user);
            res.json( {message : "success" , data : user });
        })
        .catch(err => {
            res.status(500).json({message : err});
        })
        .finally(()=>{
            db.disconnect();
        });

    
}


exports.createUser = (req,res,next) => {

    User.create({
        id : req.params.id
        ,pwd : 1234
        ,crt_dt : new Date()
        ,updt_dt : new Date()
    })
    .then( createResult => {
        console.log(createResult);
        res.json({message : "create User" , data : createResult });
    })
    .catch(err => {
        res.status(500).json({message : err});
    }).finally(()=>{
        db.disconnect();
    });

}

