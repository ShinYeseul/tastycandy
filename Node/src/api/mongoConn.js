const mongoose = require('mongoose');
const uri = "mongodb+srv://" + process.env.mongoHost + ":" + process.env.mongoPassword + "@cluster1.dmasx.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";

// If the connection throws an error
mongoose.connection.on("error", function(err) {
  console.error('Failed to connect to DB on startup ', err);
});

// When the connection is disconnected
mongoose.connection.on('disconnected', function () {
  console.log('Mongoose default connection to DB disconnected');
});

var gracefulExit = function() { 
  mongoose.connection.close(function () {
    console.log('Mongoose default connection with DB is disconnected through app termination');
    process.exit(0);
  });
}

// If the Node process ends, close the Mongoose connection
process.on('SIGINT', gracefulExit).on('SIGTERM', gracefulExit);



exports.createConnection  = async () => {

      return await mongoose.connect(
        uri,         
      { 'useUnifiedTopology': true , 
        'useNewUrlParser':  true, 
        'useFindAndModify': false , 
        'useCreateIndex': true,
        'socketTimeoutMS': 60000,
        'connectTimeoutMS': 60000,
        'poolSize': 10
      }
    ).then(conn => {
      //cached_db = conn;
      return conn;
    }).catch((err) => {
      console.error('Something went wrong', err);
      throw err;
    });


};

module.exports.disconnect = () => {
    
    if(mongoose.connection.readyState == 1){
      console.log("disconnect mongo db");
      mongoose.disconnect();
    }else{
      console.log("already disconnected");
    }

};

