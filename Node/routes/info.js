var express = require('express');
var router = express.Router();


var controller = require('../controllers/campingInfoController');

/* GET users listing. */
router.get('/', function(req, res, next) {

    res.render('info');

});

router.post('/', controller.getApi);

module.exports = router;
