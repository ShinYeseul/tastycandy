var express = require('express');
var router = express.Router();

var controller = require( '../controllers/userController' );

/* GET users listing. */
router.get('/find/:id', controller.findUser );

router.get('/create/:id', controller.createUser);


module.exports = router;
