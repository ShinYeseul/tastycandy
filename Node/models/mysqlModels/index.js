'use strict';

const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const basename = path.basename(__filename);
const env = process.env.NODE_ENV || 'development';
//const config = require(__dirname + '/../config/config.json')[env];
const db = {};

var db_info = {
  host     : process.env.host,    // 호스트 주소
  user     : process.env.dbusername,           // mysql user
  password : process.env.password,       // mysql password
  database : process.env.database,
  dialect : process.env.dialect        // mysql 데이터베이스
};


let sequelize = new Sequelize(db_info.database, db_info.user, db_info.password, {
    host: db_info.hostname,
    //port:  3306  //기본 포트가 아닐경우
    //커넥션 풀 생성
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
  
    dialect: db_info.dialect /* 'mysql' | 'mariadb' | 'postgres' | 'mssql' 중에 하나 선택*/
  });


fs
  .readdirSync(__dirname)
  .filter(file => {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
  })
  .forEach(file => {
    console.log(file);
    const model = require(path.join(__dirname, file))(sequelize, Sequelize.DataTypes);
    db[model.name] = model;
  });

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

//모델정보를 읽어온다.
db.User = require('./listModel')(sequelize, Sequelize);
//db.Comment = require('./comment')(sequelize, Sequelize);

module.exports = db;
