const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    id: { type: String, required: true, unique: true },
    pwd: { type: String, required: true },
    crt_dt: { type: Date, required: true },
    updt_dt: { type: Date, required: true },
});







module.exports = mongoose.model('User', userSchema);

