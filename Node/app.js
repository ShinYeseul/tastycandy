
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var dotenv = require('dotenv');

//env set
dotenv.config({
  path: path.resolve(
    process.cwd(),
    process.env.NODE_ENV == "production" ? ".env" : ".env.dev"
  )
});



//세션에 대한 설정
const session = require('express-session');
const passport = require('passport');


// const Sequelize = require('sequelize');
// const SequelizeAuto = require('sequelize-auto');
// const auto = new SequelizeAuto('test','root','ehdgns154',{
//   host:'localhost',
//   port:'3306'
// });
// auto.run((err)=>{
//   if(err) throw err;
// })


var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var listRouter = require('./routes/list');
var infoRouter = require('./routes/info');

var app = express();


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// dev , short , common , combined
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));
app.use(cookieParser());

app.use(session({
  resave: false,
  saveUninitialized: false,
  secret: 'scret',
  cookie: {
    httpOnly: true,
    secure: false,
  },


  })
);





app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/list', listRouter);
app.use('/info',infoRouter);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


module.exports = app;
